/**
 * Created by nazareth on 11/23/13.
 */
public class matrices {
    private long [][] weights;
    private long[][] parent;

    public matrices(long[][] weights, long[][] parent){
        this.weights = weights;
        this.parent = parent;

    }
    public long[][] getParent(){
        return parent;
    }
    public long[][] getWeights(){
        return weights;
    }
}
