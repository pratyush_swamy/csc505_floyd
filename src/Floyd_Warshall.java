
    import java.io.BufferedReader;
    import java.io.FileReader;
    import java.util.Scanner;

    public class Floyd_Warshall {

        static int numVerts = 0;

        public static long[][] readFile(String filename) {
            Scanner scanner = null;

            long[][] matrix;
            matrix = new long[0][];
            //long[][] parent_matrix;
            //parent_matrix = new long[0][];
            try {
                scanner = new Scanner(new BufferedReader(new FileReader(filename)));


                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();

                    if (line.startsWith("c") && numVerts == 0) {
                        String[] pieces = line.split(" ");
                        numVerts = Integer.parseInt(pieces[2]);
                        System.out.println("Number of Vertices - " + numVerts);
                        matrix = new long[numVerts + 1][numVerts + 1];
                        matrix = initializeMatrix(matrix, 0);
                        continue;
                    }


                    if (line.startsWith("e")) {
                        String[] pieces = line.split(" ");
                        long weight = Long.parseLong(pieces[3]);
                        int startId = Integer.parseInt(pieces[1]);
                        int endId = Integer.parseInt(pieces[2]);
                        matrix[startId][endId] = weight;
                        continue;
                    }

                }


            } catch (Exception e) {
                System.err.println(e.getMessage());
            } finally {
                if (scanner != null)
                    scanner.close();
            }
            return matrix;
        }

        private static long[][] initializeMatrix(long[][] new_matrix, int flag) {
            int n = new_matrix.length;
            //flag == 0 is for initializing distance matrix
            if (flag == 0) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        if (i == j) {
                            new_matrix[i][j] = 0;
                        } else new_matrix[i][j] = 500000;
                    }
                }
            }
            //flag == 1 is to initialize parent matrix
            else if (flag == 1) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        new_matrix[i][j] = 0;
                    }
                }
            }
            return new_matrix;
        }

        private static matrices floydWarshall(long[][] floyd_matrix, long[][] parent_matrix) {
            int n = floyd_matrix.length;
            for (int k = 1; k < n; k++) {
                for (int i = 1; i < n; i++) {
                    for (int j = 1; j < n; j++) {
                        if (floyd_matrix[i][k] + floyd_matrix[k][j] < floyd_matrix[i][j]) {
                            floyd_matrix[i][j] = floyd_matrix[i][k] + floyd_matrix[k][j];
                            parent_matrix[i][j] = k;
                        }
                    }
                }

            }

            return new matrices(floyd_matrix, parent_matrix);
        }

        static void printMatrix(long[][] matrix) {
            int n = matrix.length;
            System.out.println("Length of the Matrix - " + n);
            for (int i = 1; i < n; i++) {
                System.out.println("");
                for (int j = 1; j < n; j++) {
                    System.out.print(matrix[i][j] + " ");
                }

            }
            System.out.println("");
        }

        public static void main(String[] args) {

            long[][] floyd_matrix = readFile("large_input.txt");
            //System.out.println("Matrix before - ");
            //printMatrix(floyd_matrix);
            long[][] parent_matrix = new long[floyd_matrix.length][floyd_matrix.length];
            parent_matrix = initializeMatrix(parent_matrix, 1);
            long start = System.currentTimeMillis();
            matrices container = floydWarshall(floyd_matrix, parent_matrix);
            long end = System.currentTimeMillis();
            System.out.println("Total Time to Run Floyd Warshall - " + (end-start) + " ms");
            /*System.out.println("Floyd Warshall Matrix - ");
            printMatrix(container.getWeights());
            System.out.println("Parent Matrix - ");
            printMatrix(container.getParent());*/
        }

    }
